public class Rectangle {
    private int a;
    private int b;

    @MyFirstAnnotation(is_3D = true)
    private int h;

    @MyFirstAnnotation()
    private String whose;

    static int count;

    public Rectangle() {
        count++;
    }

    public Rectangle(int a, int b, int h, String whose) {
        this.a = a;
        this.b = b;
        this.h = h;
        this.whose = whose;
        count++;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public String getWhose() {
        return whose;
    }

    public void setWhose(String whose) {
        this.whose = whose;
    }

    public int Perimeter(){
        return this.a + this.b;
    }

    public static double Area(int a, int b){
        return a * b;
    }

    public static void saySomething(){
        System.out.println("We are rectangles and us " + count);
    }

    public boolean is_3d(){
        if(this.h == 0)
            return false;
        else
            return true;
    }

    public void myMethod(String a, int... args){
        System.out.print(a);
        for(int i : args) System.out.print(i + " ");
        System.out.println();
    }
    public String myMethod(String... args){
        StringBuilder tmp = new StringBuilder();
        for (String s : args){
            tmp.append(s + " ");
        }
        return tmp.toString();
    }
}
