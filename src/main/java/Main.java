import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException {

        Annotation[] annotations;
        Rectangle rect1 = new Rectangle();
        Class cls = rect1.getClass();

        System.out.println("Some information about " + cls.getName() + " class:");
        System.out.println();
        System.out.println("Parrent: " + cls.getSuperclass().getName());
////////////////////////////////////////////////
        Field[] declaredFields = cls.getDeclaredFields();
        if (declaredFields.length != 0) {
            System.out.println("\nFields declared with annotations: ");
            for (Field f : declaredFields) {
                annotations = f.getDeclaredAnnotations();
                if (annotations.length != 0) {
                    for (Annotation a : annotations) {
                        System.out.print("\t*");
                        System.out.print(Modifier.toString(f.getModifiers()) + " ");
                        System.out.print(f.getType() + " ");
                        System.out.print(f.getName());
                        System.out.println(" ( " + a.toString() + " )");
                    }
                }
            }
        }
////////////////////////////////////////////////
        Method[] declaredMethods = cls.getDeclaredMethods();
        if (declaredMethods.length != 0) {
            System.out.println("\nMethods : ");
            //System.out.println(declaredMethods);
            for (Method m : declaredMethods) {
                System.out.print("\t*");
                System.out.print(Modifier.toString(m.getModifiers()) + " ");
                System.out.print(m.getReturnType() + " ");
                System.out.print(m.getName() + " (");
                for (Class c : m.getParameterTypes())
                    System.out.print(c + ",");
                System.out.print(")\n");
                        //System.out.println(" ( " + a.toString() + " )");
            }
        }
        else
            System.out.println("\tThere is no methods");
////////////////////////////////////////////////
        try {
            Field field = cls.getDeclaredField("h");
            field.setAccessible(true);
            field.set(rect1,5);                                     //h = 5

            Method method = cls.getDeclaredMethod("is_3d");
            boolean _3d = (Boolean) method.invoke(rect1);
            //System.out.println(_3d);

            method = cls.getDeclaredMethod("myMethod", String[].class);
            method.invoke(rect1,new Object[] {new String []{"1,2","8,9"}});

        } catch (NoSuchMethodException e) {
            System.out.println("No such method!");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
